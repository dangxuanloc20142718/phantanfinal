-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 25, 2018 lúc 04:13 PM
-- Phiên bản máy phục vụ: 10.1.33-MariaDB
-- Phiên bản PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webphantan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'xâxxaxa', 'xâxaxaxx', NULL, NULL),
(2, 'xâxaxa', 'xâxaxax', NULL, NULL),
(3, 'xâxxx', 'xâxaxaxx', NULL, NULL),
(4, 'xâxaxax', 'xâxaxaxa', NULL, NULL),
(5, 'xâxaxax', 'xâxaaxax', NULL, NULL),
(6, 'xâxaxa', 'xâxaxaxax', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `conversation`
--

CREATE TABLE `conversation` (
  `id` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `conversation`
--

INSERT INTO `conversation` (`id`, `id_permission`, `time`, `time_end`, `content`, `created_at`, `updated_at`) VALUES
(21, 66, '2018-11-16 17:08:31', '2018-11-23 20:26:24', 'hello word', '2018-11-16 17:08:31', '2018-11-16 17:08:31'),
(22, 67, '2018-11-16 17:08:22', '2018-11-23 20:26:24', '<p>ahihi</p>', '2018-11-16 17:08:22', '2018-11-25 15:07:19'),
(23, 68, '2018-11-16 17:20:18', '2018-11-23 20:26:24', '<p>abcd</p>\n\n<p>cscscscsc</p>\n\n<p>abcd</p>', '2018-11-16 17:20:18', '2018-11-24 09:41:38'),
(24, 69, '2018-11-16 17:07:58', '2018-11-23 20:26:24', '<p>ahihi</p>', '2018-11-16 17:07:58', '2018-11-25 15:05:42'),
(57, 96, '2018-11-12 10:05:35', '2018-11-23 20:26:24', 'đặng xuân lộc', '2018-11-12 10:05:35', '2018-11-12 10:05:35'),
(58, 97, '2018-11-12 09:52:13', '2018-11-23 20:26:24', 'hello word', '2018-11-12 09:52:13', '2018-11-12 09:52:13'),
(59, 98, '2017-10-23 13:30:22', '2018-11-23 20:26:24', 'hello', '2018-11-12 09:47:01', '2018-11-12 09:47:01'),
(60, 105, '2017-10-23 13:30:11', '2017-10-23 13:30:15', 'Sao nhiều đứa xấu xí <span class=\"marker\">cứ</span> <span class=\"marker\">thích</span> <span class=\"marker\">chụp</span> <span class=\"marker\">ảnh</span> <span class=\"marker\">tự</span> <span class=\"marker\">sướng</span> <span class=\"marker\">nhỉ</span> </br>Sao nhiều đứa xấu xí <span class=\"marker\">thích</span> <span class=\"marker\">chụp</span> <span class=\"marker\">ảnh</span> <span class=\"marker\">tự</span> <span class=\"marker\">sướng</span> <span class=\"marker\">nhỉ</span> ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(61, 105, '2017-10-23 13:30:20', '2017-10-23 13:30:27', 'Thì ngoài bản thân tụi nó <span class=\"marker\">ra,</span> <span class=\"marker\">đâu</span> <span class=\"marker\">có</span> <span class=\"marker\">ai</span> <span class=\"marker\">muốn</span> <span class=\"marker\">chụp</span> <span class=\"marker\">tụi</span> <span class=\"marker\">nó</span> <span class=\"marker\">nữa</span> </br>Thì ngoài bản thân tụi nó <span class=\"marker\">ra,muốn</span> <span class=\"marker\">chụp</span> <span class=\"marker\">tụi</span> <span class=\"marker\">nó</span> <span class=\"marker\">nữa</span> ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(62, 105, '2017-10-23 13:30:30', '2017-10-23 13:30:35', 'Nếu mình cho cậu 10 triệu, cậu có dám <span class=\"marker\">ăn</span> <span class=\"marker\">***</span> <span class=\"marker\">không</span> </br>Nếu mình cho cậu 10 triệu, cậu có dám <span class=\"marker\">không</span> ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(63, 106, '2017-10-23 13:30:25', '2017-10-23 13:30:25', '<p>Anh c&oacute; thể mời em ăn tối kh&ocirc;ng?&rdquo;<br />\n-&ldquo;T&ocirc;i kh&ocirc;ng c&oacute; th&oacute;i quen d&ugrave;ng bữa với người lạ.&rdquo;<br />\n-&ldquo;Kh&ocirc;ng sao, em c&oacute; thể ngồi nh&igrave;n anh ăn m&agrave;</p>', '2018-11-25 14:41:53', '2018-11-25 14:58:42'),
(64, 106, '2017-10-23 13:30:18', '2017-10-23 13:30:40', 'Yêu từ cái nhìn đầu tiên <span class=\"marker\">là</span> <span class=\"marker\">cảm</span> <span class=\"marker\">giác</span> <span class=\"marker\">thế</span> <span class=\"marker\">nào</span> <span class=\"marker\">nhỉ</span> </br>Yêu từ cái nhìn đầu tiên <span class=\"marker\">thế</span> <span class=\"marker\">nào</span> <span class=\"marker\">nhỉ</span> ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(65, 106, '2017-10-23 13:30:30', '2017-10-23 13:30:44', 'Mở ví lôi pô li me ra nhìn là biết </br>Mở ví lôi pô li me ra nhìn là biết ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(66, 107, '2017-10-23 13:30:30', '2017-10-23 13:30:40', 'Mãi không có bạn gái, chẳng lẽ yêu cầu của mình quá cao sao </br>Mãi không có bạn gái, chẳng lẽ yêu cầu của mình quá cao sao ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(67, 107, '2017-10-23 13:30:39', '2017-10-23 13:30:47', 'Đừng ngốc nữa, là người ta yêu <span class=\"marker\">cầu</span> <span class=\"marker\">cao</span> <span class=\"marker\">đó</span> </br>Đừng ngốc nữa, là người ta yêu <span class=\"marker\">cao</span> <span class=\"marker\">đó</span> ', '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(68, 107, '2017-10-23 13:30:52', '2017-10-23 13:30:59', '26 tuổi mới bắt đầu học piano có muộn không nhỉ </br>26 tuổi mới bắt đầu học piano có muộn không nhỉ ', '2018-11-25 14:41:54', '2018-11-25 14:41:54'),
(69, 108, '2017-10-23 13:30:11', '2017-10-23 13:30:15', '<p>abcdef</p>', '2018-11-25 15:10:00', '2018-11-25 15:10:25'),
(70, 108, '2017-10-23 13:30:20', '2017-10-23 13:30:27', 'Thì ngoài bản thân tụi nó <span class=\"marker\">ra,</span> <span class=\"marker\">đâu</span> <span class=\"marker\">có</span> <span class=\"marker\">ai</span> <span class=\"marker\">muốn</span> <span class=\"marker\">chụp</span> <span class=\"marker\">tụi</span> <span class=\"marker\">nó</span> <span class=\"marker\">nữa</span> </br>Thì ngoài bản thân tụi nó <span class=\"marker\">ra,muốn</span> <span class=\"marker\">chụp</span> <span class=\"marker\">tụi</span> <span class=\"marker\">nó</span> <span class=\"marker\">nữa</span> ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(71, 108, '2017-10-23 13:30:30', '2017-10-23 13:30:35', 'Nếu mình cho cậu 10 triệu, cậu có dám <span class=\"marker\">ăn</span> <span class=\"marker\">***</span> <span class=\"marker\">không</span> </br>Nếu mình cho cậu 10 triệu, cậu có dám <span class=\"marker\">không</span> ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(72, 109, '2017-10-23 13:30:25', '2017-10-23 13:30:25', 'Mình sẽ ăn đến khi cậu sạt nghiệp mới thôi </br>Mình sẽ ăn đến khi cậu sạt nghiệp mới thôi ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(73, 109, '2017-10-23 13:30:18', '2017-10-23 13:30:40', 'Yêu từ cái nhìn đầu tiên <span class=\"marker\">là</span> <span class=\"marker\">cảm</span> <span class=\"marker\">giác</span> <span class=\"marker\">thế</span> <span class=\"marker\">nào</span> <span class=\"marker\">nhỉ</span> </br>Yêu từ cái nhìn đầu tiên <span class=\"marker\">thế</span> <span class=\"marker\">nào</span> <span class=\"marker\">nhỉ</span> ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(74, 109, '2017-10-23 13:30:30', '2017-10-23 13:30:44', 'Mở ví lôi pô li me ra nhìn là biết </br>Mở ví lôi pô li me ra nhìn là biết ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(75, 110, '2017-10-23 13:30:30', '2017-10-23 13:30:40', 'Mãi không có bạn gái, chẳng lẽ yêu cầu của mình quá cao sao </br>Mãi không có bạn gái, chẳng lẽ yêu cầu của mình quá cao sao ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(76, 110, '2017-10-23 13:30:39', '2017-10-23 13:30:47', 'Đừng ngốc nữa, là người ta yêu <span class=\"marker\">cầu</span> <span class=\"marker\">cao</span> <span class=\"marker\">đó</span> </br>Đừng ngốc nữa, là người ta yêu <span class=\"marker\">cao</span> <span class=\"marker\">đó</span> ', '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(77, 110, '2017-10-23 13:30:52', '2017-10-23 13:30:59', '26 tuổi mới bắt đầu học piano có muộn không nhỉ </br>26 tuổi mới bắt đầu học piano có muộn không nhỉ ', '2018-11-25 15:10:00', '2018-11-25 15:10:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_conversation` int(11) NOT NULL,
  `content_edit` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `history`
--

INSERT INTO `history` (`id`, `id_user`, `id_conversation`, `content_edit`, `created_at`, `updated_at`) VALUES
(75, 1, 24, '<p>t&ocirc;i t&ecirc;n l&agrave;&nbsp;</p>', '2018-11-25 15:05:36', '2018-11-25 15:05:36'),
(77, 1, 24, '<p>ahihi</p>', '2018-11-25 15:06:09', '2018-11-25 15:06:09'),
(78, 1, 22, 'xin chào mọi người', '2018-11-25 15:07:20', '2018-11-25 15:07:20'),
(79, 1, 69, 'Sao nhiều đứa xấu xí <span class=\"marker\">cứ</span> <span class=\"marker\">thích</span> <span class=\"marker\">chụp</span> <span class=\"marker\">ảnh</span> <span class=\"marker\">tự</span> <span class=\"marker\">sướng</span> <span class=\"marker\">nhỉ</span> </br>Sao nhiều đứa xấu xí <span class=\"marker\">thích</span> <span class=\"marker\">chụp</span> <span class=\"marker\">ảnh</span> <span class=\"marker\">tự</span> <span class=\"marker\">sướng</span> <span class=\"marker\">nhỉ</span> ', '2018-11-25 15:10:25', '2018-11-25 15:10:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_11_02_213038_create_articles_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_report` int(11) NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission`
--

INSERT INTO `permission` (`id`, `id_user`, `id_report`, `permission`, `created_at`, `updated_at`) VALUES
(66, 1, 21, 2, '2018-10-16 03:59:39', '2018-10-15 19:25:41'),
(67, 2, 21, 0, '2018-11-16 14:53:01', '2018-10-15 19:25:41'),
(68, 3, 21, 1, '2018-10-30 03:40:26', '2018-10-15 19:25:41'),
(69, 4, 21, 1, '2018-10-22 13:30:06', '2018-10-15 19:25:42'),
(79, 5, 21, 0, '2018-10-24 06:02:45', '2018-10-24 06:02:45'),
(96, 1, 93, 0, '2018-11-12 09:47:00', '2018-11-12 09:47:00'),
(97, 2, 93, 2, '2018-11-12 09:47:01', '2018-11-12 09:47:01'),
(98, 3, 93, 0, '2018-11-12 09:47:01', '2018-11-12 09:47:01'),
(101, 7, 21, 1, '2018-11-16 14:54:51', '2018-11-15 18:14:00'),
(105, 1, 104, 2, '2018-11-25 14:41:52', '2018-11-25 14:41:52'),
(106, 2, 104, 0, '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(107, 3, 104, 0, '2018-11-25 14:41:53', '2018-11-25 14:41:53'),
(108, 1, 105, 2, '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(109, 2, 105, 0, '2018-11-25 15:10:00', '2018-11-25 15:10:00'),
(110, 3, 105, 0, '2018-11-25 15:10:00', '2018-11-25 15:10:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullcontent` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `report`
--

INSERT INTO `report` (`id`, `name`, `content`, `fullcontent`, `created_at`, `updated_at`) VALUES
(21, 'Hệ phân tán', 'aaaaa', '<h3><span style=\"font-size:12px\">C&ocirc;ng ty.............................&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>CỘNG HO&Agrave; X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></span></h3>\r\n\r\n<p><span style=\"font-size:12px\">...............................................&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;H&ocirc;m nay, v&agrave;o l&uacute;c .......... giờ, ng&agrave;y .............................. tại C&ocirc;ng ty ............................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Giấy chứng nhận đăng k&yacute; kinh doanh số ....................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Do Ph&ograve;ng đăng k&yacute; kinh doanh &ndash; Sở kế hoạch v&agrave; đầu tư ....................... cấp ng&agrave;y .......</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Địa chỉ trụ sở ch&iacute;nh: .................................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;C&ocirc;ng ty tiến h&agrave;nh họp ...............................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Nội dung, chương tr&igrave;nh họp: .&nbsp;....................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&agrave;nh phần tham dự: ...............................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Chủ toạ: .................................................... Thư k&yacute;: .................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Diễn biến cuộc họp:</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"container-fluid\">\r\n<div class=\"row\">\r\n<div class=\"col-lg-12\">\r\n<table border=\"1px\" cellspacing=\"0\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th>Name</th>\r\n			<th>Time</th>\r\n			<th>Content</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Đặng Quốc Phương</td>\r\n			<td>2018-11-17 00:07:58</td>\r\n			<td>\r\n			<p>t&ocirc;i t&ecirc;n l&agrave;&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Đặng Phương Anh</td>\r\n			<td>2018-11-17 00:08:22</td>\r\n			<td>xin ch&agrave;o mọi người</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Đặng Xu&acirc;n Lộc</td>\r\n			<td>2018-11-17 00:08:31</td>\r\n			<td>hello word</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Đặng Văn Thọ</td>\r\n			<td>2018-11-17 00:20:18</td>\r\n			<td>\r\n			<p>abcd</p>\r\n\r\n			<p>cscscscsc</p>\r\n\r\n			<p>abcd</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Biểu quyết:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Số phiếu t&aacute;n th&agrave;nh: ........................... % số phiếu</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Số phiếu kh&ocirc;ng t&aacute;n th&agrave;nh: ........................... phiếu</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cuộc họp kết th&uacute;c l&uacute;c ............. giờ c&ugrave;ng ng&agrave;y, nội dung thảo luận tại cuộc họp đ&atilde; được c&aacute;c th&agrave;nh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;vi&ecirc;n&nbsp;dự họp th&ocirc;ng&nbsp; &nbsp;qua v&agrave; c&ugrave;ng k&yacute; v&agrave;o bi&ecirc;n bản.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bi&ecirc;n bản được c&aacute;c th&agrave;nh vi&ecirc;n nhất tr&iacute; th&ocirc;ng qua v&agrave; c&oacute; hiệu lực kể từ ng&agrave;y k&yacute;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<strong>Chữ k&yacute; của c&aacute;c th&agrave;nh vi&ecirc;n</strong></p>', '2018-11-25 14:20:44', '2018-11-25 14:20:44'),
(24, 'test', '<p>adsadsad</p>', NULL, '2018-11-05 10:50:38', '2018-11-05 10:50:38'),
(25, 'test', '<p>ffffff</p>', NULL, '2018-11-05 10:53:17', '2018-11-05 10:53:17'),
(26, 'test', '<p>qqqq</p>', NULL, '2018-11-05 10:54:56', '2018-11-05 10:54:56'),
(29, '12346', '<p>cscscsc</p>', NULL, '2018-11-05 10:59:43', '2018-11-05 10:59:43'),
(33, 'đặng xuân lộc', '<p>sasasasa</p>', NULL, '2018-11-05 16:17:21', '2018-11-05 16:17:21'),
(37, 'xâx', '<p>x&acirc;xaxa</p>', NULL, '2018-11-05 16:39:30', '2018-11-05 16:39:30'),
(38, 'cscscscsc', '<p>cscscsc</p>', NULL, '2018-11-05 16:40:12', '2018-11-05 16:40:12'),
(39, 'dsdsd', '<p>dsdsdsd</p>', NULL, '2018-11-05 16:41:26', '2018-11-05 16:41:26'),
(40, 'dsdsd', '<p>dsdsdsd</p>', NULL, '2018-11-05 16:42:42', '2018-11-05 16:42:42'),
(41, 'dsdsd', '<p>dsdsdsd</p>', NULL, '2018-11-05 16:42:59', '2018-11-05 16:42:59'),
(42, 'âsasasa', '<p>s&acirc;sasasa</p>', NULL, '2018-11-05 16:47:24', '2018-11-05 16:47:24'),
(43, 'âsasasa', '<p>s&acirc;sasasa</p>', NULL, '2018-11-05 16:48:57', '2018-11-05 16:48:57'),
(44, 'âsasasa', '<p>s&acirc;sasasa</p>', NULL, '2018-11-05 16:51:04', '2018-11-05 16:51:04'),
(45, 'âsasasa', '<p>s&acirc;sasasa</p>', NULL, '2018-11-05 16:51:19', '2018-11-05 16:51:19'),
(46, 'âsasasa', '<p>s&acirc;sasasa</p>', NULL, '2018-11-05 16:51:34', '2018-11-05 16:51:34'),
(47, 'dsdsds', '<p>dsdsdsd</p>', NULL, '2018-11-05 16:52:11', '2018-11-05 16:52:11'),
(48, 'dsdsds', '<p>dsdsdsd</p>', NULL, '2018-11-05 16:52:32', '2018-11-05 16:52:32'),
(49, 'ddd', '<p>ddđ</p>', NULL, '2018-11-05 16:53:04', '2018-11-05 16:53:04'),
(50, 'dangxuanloc lộc', '<p>lllllllllll</p>', NULL, '2018-11-05 16:57:56', '2018-11-05 16:57:56'),
(51, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:36:34', '2018-11-06 13:36:34'),
(52, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:37:59', '2018-11-06 13:37:59'),
(53, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:44:31', '2018-11-06 13:44:31'),
(54, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:45:32', '2018-11-06 13:45:32'),
(55, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:45:36', '2018-11-06 13:45:36'),
(56, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:45:52', '2018-11-06 13:45:52'),
(57, 'gggg', '<p>ggggg</p>', NULL, '2018-11-06 13:45:57', '2018-11-06 13:45:57'),
(58, 'dsdsd', '<p>hhhhh</p>', NULL, '2018-11-06 13:46:15', '2018-11-06 13:46:15'),
(59, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:46:55', '2018-11-06 13:46:55'),
(60, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:47:28', '2018-11-06 13:47:28'),
(61, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:47:48', '2018-11-06 13:47:48'),
(62, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:48:14', '2018-11-06 13:48:14'),
(63, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:48:19', '2018-11-06 13:48:19'),
(64, 'đặng xuân lộc', '<p>nnnnn</p>', NULL, '2018-11-06 13:48:29', '2018-11-06 13:48:29'),
(65, 'đặng xuân lộc', '<p>nnnn</p>', NULL, '2018-11-06 13:49:01', '2018-11-06 13:49:01'),
(66, 'đặng xuân lộc', '<p>nnnn</p>', NULL, '2018-11-06 13:49:29', '2018-11-06 13:49:29'),
(67, 'dsdsd', '<p>dsdsdsd</p>', NULL, '2018-11-06 13:50:02', '2018-11-06 13:50:02'),
(68, 'dsdsd', '<p>dsdsdsd</p>', NULL, '2018-11-06 13:51:11', '2018-11-06 13:51:11'),
(69, '20142718@student.hust.edu.vn', '<p>dsdsdsds</p>', NULL, '2018-11-06 13:54:36', '2018-11-06 13:54:36'),
(71, '20142718@student.hust.edu.vn', '<p>qqqqq</p>', NULL, '2018-11-06 13:58:51', '2018-11-06 13:58:51'),
(72, '20142718@student.hust.edu.vn', '<p>qqqqq</p>', NULL, '2018-11-06 13:59:39', '2018-11-06 13:59:39'),
(73, '20142718@student.hust.edu.vn', '<p>qqqqq</p>', NULL, '2018-11-06 14:00:04', '2018-11-06 14:00:04'),
(74, 'đặng xuân lộc', '<p>vdvdvdv</p>', NULL, '2018-11-06 14:00:49', '2018-11-06 14:00:49'),
(75, 'đặng xuân lộc', '<p>vdvdvdv</p>', NULL, '2018-11-06 14:01:33', '2018-11-06 14:01:33'),
(76, 'đặng xuân lộc', '<p>vdvdvdv</p>', NULL, '2018-11-06 14:01:45', '2018-11-06 14:01:45'),
(77, 'đặng xuân lộc', '<p>vdvdvdv</p>', NULL, '2018-11-06 14:02:55', '2018-11-06 14:02:55'),
(78, 'đặng xuân lộc', '<p>vdvdvdv</p>', NULL, '2018-11-06 14:03:10', '2018-11-06 14:03:10'),
(79, 'đặng xuân lộc', '<p>vvvvvvvvvv</p>', NULL, '2018-11-06 14:05:21', '2018-11-06 14:05:21'),
(81, 'fdfdf', '<p>dsdssd</p>', NULL, '2018-11-06 14:09:49', '2018-11-06 14:09:49'),
(84, 'phantan', 'dsadsdsadda', NULL, '2018-11-12 09:07:33', '2018-11-12 09:07:33'),
(85, 'đâsđsada', 'dấdsd', NULL, '2018-11-12 09:10:35', '2018-11-12 09:10:35'),
(86, 'đâsđsada', 'dấdsd', NULL, '2018-11-12 09:12:13', '2018-11-12 09:12:13'),
(87, 'đâsđsada', 'dấdsd', NULL, '2018-11-12 09:13:53', '2018-11-12 09:13:53'),
(90, 'đặng xuân lộc', 'cscscscc', NULL, '2018-11-12 09:17:01', '2018-11-12 09:17:01'),
(91, 'yes', 'yes', NULL, '2018-11-12 09:42:35', '2018-11-12 09:42:35'),
(92, 'yes', 'yes', NULL, '2018-11-12 09:43:39', '2018-11-12 09:43:39'),
(93, 'yes', 'yes', '<div class=\"container-fluid\">\n<div class=\"row\">\n<div class=\"col-lg-12\">\n<table border=\"1px\" cellspacing=\"0\" style=\"width:100%\">\n	<thead>\n		<tr>\n			<th>Name</th>\n			<th>Time</th>\n			<th>Content</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>Đặng Văn Thọ</td>\n			<td>2017-10-23 20:30:22</td>\n			<td>hello</td>\n		</tr>\n		<tr>\n			<td>Đặng Phương Anh</td>\n			<td>2018-11-12 16:52:13</td>\n			<td>hello word</td>\n		</tr>\n		<tr>\n			<td>Đặng Xu&acirc;n Lộc</td>\n			<td>2018-11-12 17:05:35</td>\n			<td>đặng xu&acirc;n lộc</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n</div>\n</div>', '2018-11-19 18:06:39', '2018-11-19 18:06:39'),
(95, 'cscscs', 'cscscscscsc', NULL, '2018-11-23 17:14:08', '2018-11-23 17:14:08'),
(96, 'cscscs', 'cscscscscsc', NULL, '2018-11-23 17:42:35', '2018-11-23 17:42:35'),
(97, 'cscscs', 'cscscscscsc', NULL, '2018-11-23 17:43:18', '2018-11-23 17:43:18'),
(98, 'cscscs', 'cscscscscsc', NULL, '2018-11-23 17:46:37', '2018-11-23 17:46:37'),
(99, 'cacscscs', 'xsacasc', NULL, '2018-11-23 20:05:59', '2018-11-23 20:05:59'),
(100, 'đwdwdw', 'dsdwdwdwdw', NULL, '2018-11-23 20:21:08', '2018-11-23 20:21:08'),
(101, 'đwdwdw', 'dsdwdwdwdw', NULL, '2018-11-23 20:22:09', '2018-11-23 20:22:09'),
(102, 'đwdwdw', 'dsdwdwdwdw', NULL, '2018-11-23 20:29:05', '2018-11-23 20:29:05'),
(104, 'Truyện cười', 'Những mẩu đối thoại hài hước trên mạng', NULL, '2018-11-25 14:41:52', '2018-11-25 14:41:52'),
(105, 'Phân tán', 'abcxyz', NULL, '2018-11-25 15:09:59', '2018-11-25 15:09:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` text COLLATE utf8_unicode_ci NOT NULL,
  `footer` text COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `template`
--

INSERT INTO `template` (`id`, `name`, `content`, `header`, `footer`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Họp lớp Cntt2.4', '<p>Cuộc hợp cuối kh&oacute;a</p>', '<h3><span style=\"font-size:12px\"><span class=\"marker\">C&ocirc;ng ty..</span>...........................&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>CỘNG HO&Agrave; X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></span></h3>\r\n\r\n<p><span style=\"font-size:12px\">...............................................&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</strong></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;H&ocirc;m nay, v&agrave;o l&uacute;c .......... giờ, ng&agrave;y .............................. tại C&ocirc;ng ty ............................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Giấy chứng nhận đăng k&yacute; kinh doanh số ....................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Do Ph&ograve;ng đăng k&yacute; kinh doanh &ndash; Sở kế hoạch v&agrave; đầu tư ....................... cấp ng&agrave;y .......</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Địa chỉ trụ sở ch&iacute;nh: .................................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;C&ocirc;ng ty tiến h&agrave;nh họp ...............................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Nội dung, chương tr&igrave;nh họp: .<span style=\"display:none\">&nbsp;</span>....................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&agrave;nh phần tham dự: ...............................................................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Chủ toạ: .................................................... Thư k&yacute;: .................................................</span></p>\r\n\r\n<p><span style=\"font-size:12px\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Diễn biến cuộc họp:</span></p>', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Biểu quyết:</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Số phiếu t&aacute;n th&agrave;nh: ........................... % số phiếu</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Số phiếu kh&ocirc;ng t&aacute;n th&agrave;nh: ........................... phiếu</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cuộc họp kết th&uacute;c l&uacute;c ............. giờ c&ugrave;ng ng&agrave;y, nội dung thảo luận tại cuộc họp đ&atilde; được c&aacute;c th&agrave;nh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;vi&ecirc;n&nbsp;dự họp th&ocirc;ng&nbsp; &nbsp;qua v&agrave; c&ugrave;ng k&yacute; v&agrave;o bi&ecirc;n bản.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bi&ecirc;n bản được c&aacute;c th&agrave;nh vi&ecirc;n nhất tr&iacute; th&ocirc;ng qua v&agrave; c&oacute; hiệu lực kể từ ng&agrave;y k&yacute;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<strong>Chữ k&yacute; của c&aacute;c th&agrave;nh vi&ecirc;n</strong></p>', 1, '2018-11-24 07:42:37', '2018-11-24 07:42:37'),
(2, 'Hệ phân tán', 'xxxsx', '<p>xxasxasxsa</p>', '<p>xẫccccccccc</p>', 1, '2018-11-11 13:54:00', '2018-11-11 13:54:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Đặng Xuân Lộc', 'dangxuanloc@gmail.com', NULL, '$2y$10$tzO186DR2qyIPWE/JNIGt.Jqni0CLBJaCwxaWysOFuuQgEvH2zUZm', 'Xujf4mJvRTVzfAVmcOri6QwginytuLLSMwslt8gBkEaqCkFideDzLejBxyVU', '2018-10-15 14:35:42', '2018-10-18 17:43:39'),
(2, 'Đặng Phương Anh', 'dangphuonganh@gmail.com', NULL, '$2y$10$qh3S6z0PRf4d/OszDs5O0urVxAC4yQHcR6SfeP.V.kApRO1s9MaoO', NULL, NULL, '2018-10-18 04:23:09'),
(3, 'Đặng Văn Thọ', 'dangvantho@gmail.com', NULL, '$2y$10$lqBHUqM68ng9T5DXWnplZOWEmJCZM8Slwkyw6BTHsWWvLvHZk0IrW', NULL, NULL, NULL),
(4, 'Đặng Quốc Phương', 'dangquocphuong@gmail.com', NULL, '$2y$10$19Gl/NRHvOd9OpZX1EfJsepZetPVERPb2PzjDivVzy8MAG6MNhF1C', NULL, NULL, NULL),
(5, 'Đặng Văn A', 'dangxuanloc96@gmail.com', NULL, '$2y$10$aNxh73H0p7GcljukZpQR/udbPPhZujuMCPUwSj.rAIAksYWnM1WKm', NULL, '2018-10-22 15:30:02', '2018-10-22 15:30:02'),
(6, 'Đặng Văn C', 'C@gmail.com', NULL, '', NULL, NULL, NULL),
(7, 'Trần văn A', 'tranvana@gmail.com', NULL, '$2y$10$PIdS/2D6rt5Dv9R.m9zebO3VyX0CyFQgZp78mc42b.3pvFrkJQ8eC', NULL, '2018-11-15 16:24:22', '2018-11-15 16:24:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT cho bảng `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT cho bảng `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT cho bảng `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
