<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('getlogin','LoginController@getLogin');
Route::post('postlogin','LoginController@postLogin');

Route::get('getlogout',function(){
	if(Session::has('user')){
		Session::flush();
		return redirect('getlogin');
	}
});

Route::get('getregister','RegisterController@getRegister');
Route::post('postregister','RegisterController@postRegister');

Route::group(['middleware' => 'member'],function(){
	Route::resource('report','ReportController');
	Route::get('share_authorize/{id_report}','ShareAuthorizeController@getShare');
	Route::post('share_authorize','ShareAuthorizeController@searchUser');
	Route::get('add_share_authorize/{id_user}/{id_report}','ShareAuthorizeController@addAuthorize');
	Route::get('delete_share_authorize/{id_user}/{id_report}','ShareAuthorizeController@deleteAuthorize');
	Route::get('permission','PermissionController@getPermission');
	Route::post('permission','PermissionController@postPermission');
	Route::get('conversation/{id}','ConversationController@getConversation');
	Route::post('conversation','ConversationController@postConversation');
	Route::post('save_content_report','ConversationController@SaveContent');
	Route::get('update_content_report/{id_report}','ConversationController@updateContent');
	Route::post('update_realtime_content_report','ConversationController@updateRealtimeContent');
	Route::get('get_report_content/{id}','ConversationController@getReportContent');
	Route::get('change_template/{id_report}','ConversationController@getChangeTemplate');
	Route::post('change_template','ConversationController@postChangeTemplate');
	Route::get('information','InformationController@getInformation');
	Route::post('information','InformationController@postInformation');
	Route::resource('template','TemplateController');
	Route::get('delete_all_template','DeleteAllTemplateController@deleteAll');
	Route::get('authorize/{id}','AuthorizeController@getAuthorize');
	Route::post('authorize','AuthorizeController@postAuthorize');
	Route::get('history/{id}','HistoryController@getHistory');
	Route::get('restore/{id}','HistoryController@getRestore');
	Route::get('delete_history/{id}','HistoryController@deleteHistory');
	Route::get('delete_all_history/{id}','HistoryController@deleteAllHistory');
	Route::post('exportfile','ExportFileController@postExportFile');
	Route::get('statistic/user','StatisticController@statisticUser');
	Route::get('statistic/user/{id}','StatisticController@statisticUserDetail');
	Route::get('statistic/report','StatisticController@statisticReport');
	Route::get('statistic/report/{id}','StatisticController@statisticReportDeTail');
	Route::get('statistic/{id_report}/{id_user}','StatisticController@statisticDetail');
});
Auth::routes();

	
   
