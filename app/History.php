<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table='history';
    protected $fillable=[
    	'id_user',
    	'id_conversation',
    	'content_edit'
    ];
    public function conversation(){
    	return $this->belongsTo('App\Conversation','id_conversation','id');
    }
    public function user(){
    	return $this->belongsTo('App\User','id_user','id');
    }

}
