<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table='report';
    protected $fillable=[
    	'name',
    	'content'
    ];
    public function user(){
    	return $this->belongsTo('App\User','id_user','id');
    }
    public function permission(){
    	return $this->hasMany('App\Permission','id_report','id');
    }
}
