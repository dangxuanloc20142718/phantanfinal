<?php
	function read_file_docx($filename){
		$striped_content = '';
        $content = '';
        if(!$filename || !file_exists($filename)) return false;
        $zip = zip_open($filename);
        if (!$zip || is_numeric($zip)) return false;
        while ($zip_entry = zip_read($zip)) {
            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
            if (zip_entry_name($zip_entry) != "word/document.xml") continue;
            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
            zip_entry_close($zip_entry);
        }
        zip_close($zip);
        $striped_content = strip_tags($content);
        return $striped_content;
	}
    function getTime_or_Id_or_Content($string,$number){
            $mang = explode ('+',$string);
            if($number == 1){
                $time = substr($mang[1],5);
                return $time;
            }else{
                $id_or_content = substr($mang[0],8);
                return $id_or_content;
            }
            
        }
    // function array_string($string){
    //         $arr1 = explode('[',$string);
    //         array_shift($arr1 );
    //         $arr2 = [];
    //         foreach ($arr1 as $value) {
    //             $arr2[] = trim($value,']');
    //         }
    //         return $arr2;
    // } 
    function content_file($file){
            $tail = $file->getClientOriginalExtension();
            if($tail != 'txt' && $tail != 'docx'){
                return redirect('report/create')->with('message','file bạn chọn không phải là file .txt hoặc file .docx');
            }
            $name = $file->getClientOriginalName();
            $new_name = str_random(4).'_'.$name;  
            while(file_exists('upload/sanpham'. $new_name)){
                 $new_name = str_random(4).'_'.$name;  
            }    
            $file->move('upload/sanpham',$new_name);

            $fileOpen=@fopen('upload/sanpham/'.$new_name,'r');
            $contents = "";
            if($tail == 'txt'){
                while(!feof($fileOpen)){
                $contents = $contents.trim(fgets($fileOpen));
                }
            }
            else{
                $contents = read_file_docx('upload/sanpham/'.$new_name);
            }
            return $contents;
    }
    function get_conversation($id){
        // lấy một colection các conversation
        $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.id as id','permission.permission as permission','permission.id_user as id_user_conver')
            ->where('permission.id_report',$id)
            ->orderBy('time','asc')
            ->get();
        //convert thành table html 
        $content="";
            foreach ($conversation as $val) {
                $content.='<tr>
                                <td>'.$val->username.'</td>'.
                               '<td>'.$val->time.'</td>'.
                               '<td>'.$val->content.'</td>'.
                          '</tr>';
                 }  
        $table = '<div class="container-fluid">
                     <div class="row">
                       <div class="col-lg-12">
                           <table style="width:100%" border="1px" cellspacing="0" >
                                <thead>
                                    <tr>
                                        <th>Name</td>
                                        <th>Time</td>
                                        <th>Content</td>
                                    </tr>
                                </thead>
                                <tbody>'.
                                $content
                                .'</tbody>
                            </table>
                        </div>
                    </div>
                </div>';
            return $table;
    }
    function getSearchCover($id,$id_user){
        if($id_user == 0){
            $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.time_end as time_end','conversation.id as id','permission.permission as permission','permission.id_user as id_user_conver')
            ->where('permission.id_report',$id)
            ->orderBy('time','asc')
            ->get();
            return $conversation;
        }else{
             $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.time_end as time_end','conversation.id as id','permission.permission as permission','permission.id_user as id_user_conver')
            ->where([['permission.id_report',$id],['permission.id_user',$id_user]])
            ->orderBy('time','asc')
            ->get();
            return $conversation;
        }
    }
    function getSelectUser($id_report){
       $user_per = DB::table('users')
            ->join('permission','permission.id_user','users.id')
            ->select('users.name as name','users.email as email','permission.id as id','permission.id_user as id_user')
            ->where('id_report',$id_report)
            ->get(); 
        $array_id_username = [];
        $array_id_username[0] = '---Select Name---';
        foreach ($user_per as $value) {
            $test = DB::table('conversation')->where('id_permission',$value->id)->get();
            if(count($test) > 0){
                $array_id_username[$value->id_user] = $value->name;
            }
        }
        return $array_id_username;
    }
    function arrays_string($string){
            $array = explode ('[',$string);
            $new_array = [];
            foreach ($array as $key => $value) {
                $new_array[] = rtrim($value,']');
            }
            array_shift($new_array);
            return $new_array;
        }
        function get_content_or_id($string){
            $postfirt = strpos($string,'+');
            $stringprocess = substr($string,8,$postfirt-8);
            return $stringprocess;
        }
        function get_time_start($string){
            $postfirt = strpos($string,'+');
            $start_time = substr($string,$postfirt+6,19);
            return $start_time;
        }
        function get_time_finish($string){
            $postfirt = strpos($string,'+');
            $end_time = substr($string,$postfirt+26,19);
            return $end_time;
        }
        function get_audio_frequency($string){
            $postfirt = strpos($string,'+');
            $acc = substr($string,$postfirt+50,9);
            return  $acc;
        }
        function get_label($string){
            $postfirt = strpos($string,'+');
            $label = substr($string,$postfirt+66);
            return $label;
        }
        function compare_content($string1,$string2){
            $array1_word = explode(' ',$string1);
            $array2_word = explode(' ',$string2);
            $length_array1 = count($array1_word);
            $length_array2 = count($array2_word);
            $length_min = $length_array1 <= $length_array2 ? $length_array1 : $length_array2;
            $content_merg = '';
            $content_stenograph = '';
            for ($i=0; $i < $length_min; $i++) { 
                if($array1_word[$i] == $array2_word[$i]){
                    $content_merg .= $array1_word[$i].' ';
                    $content_stenograph .= $array2_word[$i].' ';
                }else{
                    $content_merg .= '<span class="marker">'.$array1_word[$i].'</span>'.' ';
                    $content_stenograph .= '<span class="marker">'.$array2_word[$i].'</span>'.' ';
                }
            }
            if($length_array1 < $length_array2){
                for ($i = $length_array1; $i < $length_array2; $i++) { 
                    $content_stenograph .= '<span class="marker">'.$array2_word[$i].'</span>'.' ';
                }
            }else{
                for ($i = $length_array2; $i < $length_array1; $i++) { 
                    $content_merg .= '<span class="marker">'.$array1_word[$i].'</span>'.' ';
                }
            }
            return $content_merg.'</br>'.$content_stenograph;
        }
        function get_label_final($string){
            $postfirt = strpos($string,'label');
            $label = substr($string,$postfirt+6);
            return $label;
        }
        function get_content_stenograph($string){
            $postfirt = strpos($string,'+');
            $postlabel = strpos($string,'label');
            $content = substr($string,$postfirt+54,$postlabel - $postfirt -55);
            return $content;
        }
        function get_content_merg($string){
            $postfirt = strpos($string,'+');
            $postlabel = strpos($string,'ACC');
            $content = substr($string,$postfirt+54,$postlabel - $postfirt -55);
            return $content;
        }
        function quantity_user_or_report($id,$name){
            if($name == 'user'){
                $quantity = DB::table('permission')->where('id_report',$id)->get();
                return count($quantity);
            }else{
                $quantity = DB::table('permission')->where('id_user',$id)->get();
                return count($quantity);
            }
            
        }
        function get_search_keyword($id_report,$keyword){
             $conversation = DB::table('permission')
            ->join('conversation','conversation.id_permission','permission.id')
            ->join('users','users.id','permission.id_user')
            ->select('users.name as username','conversation.content as content','conversation.time as time','conversation.time_end as time_end','conversation.id as id','permission.permission as permission','permission.id_user as id_user_conver')
            ->where([['permission.id_report',$id_report],['conversation.time','like',"%$keyword%"]])
            ->orWhere([['permission.id_report',$id_report],['conversation.content','like',"%$keyword%"]])
            ->orderBy('time','asc')
            ->get();
            return $conversation;
        }

?>
