<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Report;
use DB;
class StatisticController extends Controller
{
    public function statisticUser(){
    	$user = User::all();
    	return view('statistic.statistic_user',compact('user'));
    }
    public function statisticUserDetail($id){
    	$report = DB::table('report')
    			->join('permission','permission.id_report','report.id')
    			->select('report.name as name','report.content as description','permission.permission as permission')
    			->where('id_user',$id)
    			->get();
    	return view('statistic.statistic_user_detail',compact('report'));
    }
    public function statisticReport(){
    	$report = Report::all();
    	return view('statistic.statistic_report',compact('report'));
    }
    public function statisticReportDeTail($id){
    	$user = DB::table('users')
    			->join('permission','permission.id_user','users.id')
    			->select('users.name as name','users.email as email','permission.permission as permission','users.id as id')
    			->where('id_report',$id)
    			->get();
    	$id_report = $id;
    	return view('statistic.statistic_report_detail',compact('user','id_report'));
    }
    public function statisticDetail($id_report,$id_user){
    	$id_permission = DB::table('permission')->where([['id_report',$id_report],['id_user',$id_user]])->first()->id;
    	$coversations = DB::table('conversation')->where('id_permission',$id_permission)->get();
    	return view('statistic.statistic_detail',compact('coversations'));
    }
}
