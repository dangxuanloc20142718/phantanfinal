<?php

namespace App\Http\Controllers;
use App\Http\Requests\TemplateRequest;
use Illuminate\Http\Request;
use App\Template;
use Session;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Session::get('user')->id;
        $list_temp = Template::where('id_user',$id_user)->get();
        return view('template.list',compact('list_temp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('template.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TemplateRequest $request)
    {
        $data = $request->all();
        $id_user = Session::get('user')->id;
        $data['id_user'] =  $id_user;
        $template = new Template();
        $template->create($data);
        Session::flash('message','Bạn đã thêm thành công một Template mới');
        return redirect('template/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::find($id);
        return view('template.update',compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TemplateRequest $request, $id)
    {
        $data = $request->all();
        $id_user = Session::get('user')->id;
        $data['id_user'] =  $id_user;
        $template = Template::find($id);
        $template->update($data);
        Session::flash('message','Bạn đã cập nhật thành công một Template mới');
        return redirect("template/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template = Template::find($id);
        $template->delete();
        Session::flash('message','Bạn đã xóa thành công');
        return redirect('template');
    }
}
