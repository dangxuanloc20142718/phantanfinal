<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Conversation;
use App\Events\SendMessageEvent;
use Session;
use App\Permission;
use App\Template;
use App\History;
use App\Report;
class ConversationController extends Controller
{
    public function getConversation(Request $request,$id_report){
         
        //lấy tât cả conversation của report được lựa chọn
        $id_user_select = 0;
        if($request->id_user_select != null){
            $id_user_select = $request->id_user_select;
        }
        $keyword = '';
        if($request->keyword != null){
            $keyword = $request->keyword;
        }
        if($keyword != ''){
             $conversation = get_search_keyword($id_report,$keyword);
        }else{
             $conversation = getSearchCover($id_report,$id_user_select);
        }
        $list_username = getSelectUser($id_report);
        $id_user = Session::get('user')->id;
         //lấy permisson của user đang dùng so với report vừa chọn,mục đích là xem user hiện tại có quyền sửa hay không
        $permission = Permission::where([['id_report',$id_report],['id_user',$id_user]])->get();
        $per_user = $permission[0]->permission;
         //lấy id_report
        return view('conversation.list',compact('conversation','per_user','id_report','id_user','list_username','id_user_select'));
    }
    public function postConversation(Request $request){
         if($request->ajax()){
            //update coversation
            $id_cover = $request->id;
            $new_content = $request->content;
            $new_content = str_replace('<span class="marker">','',$new_content);
            $new_content = str_replace('</span>','',$new_content);
            $old_conver = Conversation::find($id_cover);
            $old_content =  $old_conver->content;
            $old_conver->content = $new_content;
            $old_conver->save();
            //create history
            $id_user = Session::get('user')->id;
            $history = new History();
            $history->id_user = $id_user;
            $history->id_conversation = $id_cover;
            $history->content_edit =  $old_content;
            $history->save();
            event(new SendMessageEvent($old_conver));
            return ;
         }
    }
    public function getReportContent($id_report){
        $report = Report::find($id_report);
        $full_content = $report->fullcontent;
        if($full_content == null){
            $full_content = get_conversation($id_report);
            $report = Report::find($id_report);
            $report->fullcontent = $full_content;
            $report->save();
            //quyền người dùng đối với report
            $id_user = Session::get('user')->id;
            $role = Permission::where([['id_report',$id_report],['id_user',$id_user]])->first()->permission;
            return view('conversation.content',compact('report','role'));
        }
            $id_user = Session::get('user')->id;
            $role = Permission::where([['id_report',$id_report],['id_user',$id_user]])->first()->permission;
        return view('conversation.content',compact('report','role'));
    }
    public function getChangeTemplate($id_report){
        //lấy danh sách các template
        $id_user = Session::get('user')->id;
        $list_template = Template::all();
        $array_template = [];
        $array_template[0] = '---Select Template---';
         foreach ($list_template as $val) {
            $array_template[$val->id] = $val->name;
         }     
        $report = Report::find($id_report);
        //quyền người dùng đối với report
        $id_user = Session::get('user')->id;
        $role = Permission::where([['id_report',$id_report],['id_user',$id_user]])->first()->permission;
        return view('conversation.change_template',compact('report','array_template','role'));
    }
    public function postChangeTemplate(Request $request){
        if($request->ajax()){
            if($request->id_template == 0){
                return;
            }else{
                $id_template = $request->id_template;
                $id_report = $request->id_report;
                $template = Template::find($id_template);
                $header = $template->header;
                $footer = $template->footer;
                $full_content = get_conversation($id_report);
                $content = $header."</br>".$full_content.'</br>'.$footer;
                return $content;  
            }
        }
    }
    public function SaveContent(Request $request){
           $id_report = $request->id_report;
           $fullcontent = $request->fullcontent;
           $report = Report::find($id_report);
           $report->fullcontent = $fullcontent;
           $report->save();
          Session::flash('message','Bạn vừa thay đổi nội dung bản báo cáo thành công');
          return redirect("get_report_content/$id_report");
    }
    public function updateContent($id_report){
        $update_content = get_conversation($id_report);
        $report = Report::find($id_report);
        $report->fullcontent = $update_content;
        $report->save();
        Session::flash('message','Bạn vừa cập nhật nội dung report thành công');
        return redirect("get_report_content/$id_report");
    }
    public function updateRealtimeContent(Request $request){
        if($request->ajax()){
            $id_report = $request->id_report;
            $content = $request->content;
            $report = Report::find($id_report);
            $report->fullcontent = $content;
            $report->save();
            event(new SendMessageEvent($report));
            return;
        }
    }

}

