@extends("admin.layout.index")
@section("content")  
   <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Mục
                            <small>Add</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "admin/danhmuc/", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group">
                                <label>Tên danh mục</label>
                                {!! Form::text('ten_dm',old('ten_dm',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Mô tả</label>
                                {!! Form::textarea('noidung',old('noidung',''), array('class' => 'form-control ckeditor', 'id' => 'demo', 'rows' => 3)) !!}
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                       {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection