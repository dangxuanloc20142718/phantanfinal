@extends("layout.index")
@section("content")  
   <div class="right_col" role="main">
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Report
                            <small>{{$report->name}}</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12" style="padding-bottom:120px">
                        <div class="form-group">
                             {!! Form::open( ['url' => "save_content_report", 'method' => 'post', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                                    <label>Template</label>
                                    {!! Form::select('template',$array_template,null,array('class' => 'form-control template')) !!}
                        </div>
                        <center style="margin-top: 20px">
                            <div class="form-group">
                                    <h1><label>Content</h1></label>
                                    @if($role == 0)
                                        {!!Form::textarea('fullcontent','', array('class' => 'form-control abc','disabled'=>'disabled','id' => 'edit4', 'rows' => 20)) !!}
                                    @else
                                        {!!Form::textarea('fullcontent','', array('class' => 'form-control abc', 'id' => 'edit4', 'rows' => 20)) !!}
                                    @endif
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id_report" id="inputId_report" class="form-control" value="{{$report->id}}">
                            </div>
                            <div class="form-group">
                                @if($role == 0)
                                    <button type="submit" class="btn btn-primary hidden"><i class="fa fa-paper-plane"></i> Submit</button>
                                @else
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Submit</button>
                                @endif
                            </div>
                        </center>
                                     {{csrf_field()}}              
                             {!! Form::close() !!}
                    </div>
              </div>
  </div>
  <script type="text/javascript">
            config = {};
            config.language ='en';
            config.width = '650px';
            config.height = '800px';
            CKEDITOR.replace('edit4',config);
 </script>  
 <script type="text/javascript">  
            $('.template').change(function(){
            $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                 });
            $.ajax({
                    url: "change_template",
                    type: 'post',
                    data: {id_report: <?php echo $report->id ?>,
                           id_template: $(this).val()
                    },
                    success: function(data){
                        CKEDITOR.instances['edit4'].setData(data);
                    }
            });
        });
 </script>
            
@endsection


