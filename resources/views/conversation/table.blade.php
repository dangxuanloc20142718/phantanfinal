 <div class="col-lg-12" style="padding-bottom:120px">

                            @foreach($conversation as $key => $conver)
                                <div class="form-group">
                                    <label>[{{$conver->username}}] Time Start [{{$conver->time}}]
                                        Time End [{{$conver->time_end}}]
                                        @if($per_user == 1 || $per_user == 2)
                                        <a href="history/{{$conver->id}}">[History edit]</a>
                                        @endif

                                    </label>
                                    @if($per_user == 0)

                                        {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control  inputHoithoai','disabled'=>'disabled',      
                                        'id' => "$conver->id")) !!}

                                    @else

                                        {!! Form::textarea($conver->id,$conver->content, array('class' => 'form-control inputHoithoai','id' => "$conver->id")) !!}                                   
                                    @endif  
                                </div>
                            @endforeach

                                <div class="form-group">
                                        <a href="get_report_content/{{$id_report}}" style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-book"></i> Show Content</a>
                                </div>
                     
                    </div>
  <script type="text/javascript">
            config = {};
            config.language ='en';
            config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        '/',
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Radio,Checkbox,Textarea,TextField,Select,Button,ImageButton,HiddenField,NumberedList,BulletedList,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Link,Unlink,Anchor,Language,BidiRtl,BidiLtr,Image,Flash,Table,Smiley,HorizontalRule,SpecialChar,PageBreak,Iframe,TextColor,BGColor,Maximize,ShowBlocks,About';

            var conver = document.getElementsByClassName('inputHoithoai');
            for( var i=0 ; i<conver.length ; i++){
                CKEDITOR.replace($(conver[i]).attr('id'),config);
            }
            for (var j in CKEDITOR.instances) {
               CKEDITOR.instances[j].on('blur', function(e) {
                    $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                     });
                    $.ajax({
                        url: 'conversation',
                        data: {content: CKEDITOR.instances[e.editor.name].getData(),
                               id: e.editor.name
                        },
                        type: 'post'
                    });  
                    
               });
         }

 </script> 
 