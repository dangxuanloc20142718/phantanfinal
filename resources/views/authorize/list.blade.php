@extends("layout.index")
@section("content")        
             
        <div class="right_col" role="main">
            <div class="col-lg-12">
                    <h1 class="page-header">Authorize
                        <small>Grant rights</small>
                    </h1>
            </div>
            <div class="col-lg-12">
                    @if(Session::has('message'))
                        <br/>
                        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                    @endif
            </div>
                    <!-- /.col-lg-12 -->            
         
                    @include('authorize.table')
        </div>
@endsection
