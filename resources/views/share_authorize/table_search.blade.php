@if(count($users) > 0)
        <div class="col-lg-12" >
            <table class="table table-striped table-bordered table-hover">
	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Name</th>
	                                <th>Email</th>
	                                <th>Add</th>
	                            </tr>
	                        </thead>
	                        <tbody>
							  @foreach($users as $key => $user)
	                        	<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email}}</td>
									<td style="text-align: center;"><a class="btn btn-primary btn-sm addauthorize" href="add_share_authorize/{{$user->id}}/{{$id_report}}">Choose</a></td>  
								</tr>
	                         @endforeach
	                        </tbody>
	        </table>       
        </div>
   
@endif

<script type="text/javascript">
	$('.addauthorize').click(function(){
		if(!confirm("Bạn có chắc chắn muốn phân quyền cho người này ?")){
			return false;
		}	
	});
</script>