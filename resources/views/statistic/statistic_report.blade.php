@extends("layout.index")
@section("content")
    <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Table Statistic Report<small>List</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">             
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                              <tr align="center">
                                  <th>STT</th>
                                  <th>Name</th>
                                  <th>Description</th>
                                  <th>Quantity user</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($report as $key => $val)
                              <tr class="odd gradeX" align="center">
                                  <td>{{$key+1}}</td>
                                  <td>{{$val->name}}</td>
                                  <td>{!!$val->content!!}</td>
                                  <td>
                                      <a href="statistic/report/{{$val->id}}" style="color: red">{{quantity_user_or_report($val->id,'user')}}</a>      
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
 


