@extends("layout.index")
@section("content")  
   <div class="right_col" role="main">
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Dialog
                            <small>Detail</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "detail", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                             <div class="form-group">
                                @foreach($detail as $dt)
                                <label>{{$dt->id}} [{{$dt->time}}]</label>
                                {!! Form::text($dt->id,$dt->content, array('class' => 'form-control ckeditor inputHoithoai ', 'id' => 'demo', 'rows' => 3)) !!}
                                @endforeach
                            </div>
                        {!! Form::close() !!}
                            <button type="submit" class="btn btn-default" id='ajaxx'>Export Template</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                     
                    </div>
              </div>
  </div>
           
   
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script>
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher('2c618c6614c7cd6214b9', {
          cluster: 'ap1',
          encrypted: true
      });

      //Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('my-channel');

      //Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\OrderShipped', addMessage);

      function addMessage(data) {
        var id=data.message.id;
        $("[name="+id+"]").val(data.message.content);
      }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.inputHoithoai').change(function(){
                 $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                 });
                $.ajax({
                    url: 'detail',
                    data: {nd: $(this).val(),
                           id: $(this).attr('name')
                    },
                    type: 'post',
                    success: function(data){
                        console.log(data);
                    }
                });  
               
            });
           
            
        });
       
    </script>