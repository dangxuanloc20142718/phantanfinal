@extends("layout.index")
@section("content")  
    <div class="right_col" role="main">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Template
                            <small>Add </small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(count($errors)>0)
                            @foreach($errors->all() as $er)
                                <div class="alert alert-warning">
                                    <strong>Thông báo: </strong>{{$er}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "template", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group">
                                <label>Name</label>
                                {!! Form::text('name',old('name'), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                {!! Form::textarea('content',old('content'), array('class' => 'form-control', 'rows' => 3)) !!}
                            </div>
                            <center>
                                <div class="col-lg-12">
                                     <div class="form-group">
                                        <label>Header</label>
                                        {!! Form::textarea('header',old('header'), array('class' => 'form-control', 'id' => 'edit2', 'rows' => 3)) !!}
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Footer</label>
                                        {!! Form::textarea('footer',old('footer'), array('class' => 'form-control ', 'id' => 'edit3', 'rows' => 6)) !!}
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add</button>
                            </center>       
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
      

        <script type="text/javascript">
          config = {};
          config.language ='en';
          config.height = '100px';
          config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'styles', groups: [ 'styles' ] },
        '/',
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        '/',
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Source,Undo,Redo,Find,Replace,SelectAll,Form,Radio,Checkbox,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Link,Unlink,Anchor,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Save,Templates,NewPage,Preview,Cut,Copy,Paste,PasteFromWord,Image,Blockquote,CreateDiv,Outdent,Indent,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,BidiLtr,BidiRtl,TextColor,BGColor,Maximize,ShowBlocks,About,Print,PasteText';
            CKEDITOR.replace('edit1',config);
        </script>
        <script type="text/javascript">
            config = {};
            config.language ='en';
            config.width = '650px';
            config.height = '400px';
            CKEDITOR.replace('edit2',config);
        </script>
        <script type="text/javascript">
            config = {};
            config.language ='en';
            config.width = '650px';
            config.height = '400px';
            CKEDITOR.replace('edit3',config);
        </script>
@endsection


