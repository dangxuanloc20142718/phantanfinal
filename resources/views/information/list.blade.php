@extends("layout.index")
@section("content")  
   <div class="right_col" role="main">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Information
                            <small>Edit</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(count($errors)>0)
                            @foreach($errors->all() as $er)
                                <div class="alert alert-warning">
                                    <strong>Thông báo: </strong>{{$er}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                        @include('information.table')
                </div>
    </div>
        
@endsection
